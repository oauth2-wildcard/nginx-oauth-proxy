FROM nginxinc/nginx-unprivileged:1.17-alpine-perl as builder

ENV SET_MISC_VERSION     0.32
ENV NGX_DEVEL_KIT        0.3.1

USER root
run apk add --no-cache wget tar build-base pcre-dev zlib-dev libaio-dev openssl-dev libaio\
    && wget http://nginx.org/download/nginx-${NGINX_VERSION}.tar.gz -O nginx.tar.gz \
    && wget https://github.com/openresty/set-misc-nginx-module/archive/v${SET_MISC_VERSION}.tar.gz -O set-misc.tar.gz \
    && wget https://github.com/simplresty/ngx_devel_kit/archive/v${NGX_DEVEL_KIT}.tar.gz -O ngx_devel_kit.tar.gz \
    && tar -xzvf nginx.tar.gz \
    && tar -xzvf set-misc.tar.gz \
    && tar -xzvf ngx_devel_kit.tar.gz \
    && cd nginx-${NGINX_VERSION} \
    && ./configure --prefix=/etc/nginx \
        --sbin-path=/usr/sbin/nginx \
        --modules-path=/usr/lib/nginx/modules \
        --conf-path=/etc/nginx/nginx.conf \
        --error-log-path=/var/log/nginx/error.log \
        --http-log-path=/var/log/nginx/access.log \
        --pid-path=/var/run/nginx.pid \
        --lock-path=/var/run/nginx.lock \
        --http-client-body-temp-path=/var/cache/nginx/client_temp \
        --http-proxy-temp-path=/var/cache/nginx/proxy_temp \
        --http-fastcgi-temp-path=/var/cache/nginx/fastcgi_temp \
        --http-uwsgi-temp-path=/var/cache/nginx/uwsgi_temp \
        --http-scgi-temp-path=/var/cache/nginx/scgi_temp \
        --with-perl_modules_path=/usr/lib/perl5/vendor_perl \
        --user=nginx \
        --group=nginx \
        --with-compat \
        --with-threads \
        --with-http_addition_module \
        --with-http_auth_request_module \
        --with-http_dav_module \
        --with-http_flv_module \
        --with-http_gunzip_module \
        --with-http_gzip_static_module \
        --with-http_mp4_module \
        --with-http_random_index_module \
        --with-http_realip_module \
        --with-http_secure_link_module \
        --with-http_slice_module \
        --with-http_ssl_module \
        --with-http_stub_status_module \
        --with-http_sub_module \
        --with-http_v2_module \
        --with-mail \
        --with-mail_ssl_module \
        --with-stream \
        --with-stream_realip_module \
        --with-stream_ssl_module \
        --with-stream_ssl_preread_module \
        --with-cc-opt='-Os -fomit-frame-pointer' \
        --with-ld-opt=-Wl,--as-needed \
        --add-dynamic-module=../ngx_devel_kit-${NGX_DEVEL_KIT} \
        --add-dynamic-module=../set-misc-nginx-module-${SET_MISC_VERSION} \
    && make


FROM nginxinc/nginx-unprivileged:1.17-alpine-perl

USER root

COPY --from=builder nginx-1.17.2/objs/ngx_http_set_misc_module.so /usr/lib/nginx/modules/ngx_http_set_misc_module.so
COPY --from=builder nginx-1.17.2/objs/ndk_http_module.so /usr/lib/nginx/modules/ndk_http_module.so

RUN rm /etc/nginx/conf.d/* \
    && mkdir /etc/nginx/modules.d \
    && sed -i "s!events {!include /etc/nginx/modules.d/*.conf;\n\nevents {!g" /etc/nginx/nginx.conf\
    && sed -i "s!events {!env OAUTH_AUTH_URL;\nevents {!g" /etc/nginx/nginx.conf\
    && sed -i "s!events {!env VALID_REDIRECT_REGEX;\n\nevents {!g" /etc/nginx/nginx.conf

USER 101

COPY ./nginx-conf.d/* /etc/nginx/conf.d
COPY ./modules.d/* /etc/nginx/modules.d