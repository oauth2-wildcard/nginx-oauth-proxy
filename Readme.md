# Nginx Oauth2 proxy

This project does simple redirect magic to relay oauth2 tokens to your application. It is useful for Gitlab's review applications, because it is really hard to keep going to your Oauth2 auth provider developer console and keep adding and removing your newly created stagings...

It is also rootless, because I think people should get slapped, for requiring root access on docker container.

## How to use
Starting is simple:

```
$ docker run --name nginx-oauth-proxy -e OAUTH_AUTH_URL="Your oauth2 provider auth endpoint" -d registry.gitlab.com/oauth2-wildcard/nginx-oauth-proxy:latest
```

example for Google Oauth2:

```
$ docker run --name nginx-oauth-proxy -e OAUTH_AUTH_URL="https://accounts.google.com/o/oauth2/auth" -d registry.gitlab.com/oauth2-wildcard/nginx-oauth-proxy:latest
```

PS. FOR THE LOVE OF GOD, DO NOT USE IT ON PRODUCTION!!!
